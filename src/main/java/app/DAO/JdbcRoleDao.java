package app.DAO;

import app.Entity.Role;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository("roleDao")
public class JdbcRoleDao implements RoleDao {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public final void create(final Role role) {
        getSession().save(role);
    }

    @Override
    public final void update(final Role role) {
        getSession().update(role);
    }

    @Override
    public final void remove(final Role role) {
        getSession().delete(role);
    }

    @Override
    public final Role findByName(final String name) {
        Criteria criteria = createEntityCriteria();
        criteria.add(Restrictions.eq("name", name));
        Role role = (Role) criteria.uniqueResult();
        return role;
    }

    public final Role findByID(final long id) {
        return getSession().get(Role.class, id);
    }

    public final List<Role> findAll() {
        return (List<Role>) createEntityCriteria().list();
    }

    public Session getSession(){
        return sessionFactory.getCurrentSession();
    }

    public Criteria createEntityCriteria(){
        return getSession().createCriteria(Role.class);
    }

}
