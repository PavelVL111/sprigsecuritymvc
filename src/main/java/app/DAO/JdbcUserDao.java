package app.DAO;

import app.Entity.User;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository("userDao")
public class JdbcUserDao implements UserDao {
    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public  void create( User user) {
        getSession().save(user);
    }

    @Override
    public  void update(User user) {
        getSession().update(user);
    }

    @Override
    public  void remove( User user) {
        getSession().delete(user);
    }

    @Override
    public  List<User> findAll() {
        return (List<User>) createEntityCriteria().list();
    }

    @Override
    public  User findByLogin( String login) {
        Criteria criteria = createEntityCriteria();
        criteria.add(Restrictions.eq("login", login));
        return (User) criteria.uniqueResult();
    }

    @Override
    public  User findByEmail( String email) {
        Criteria criteria = createEntityCriteria();
        criteria.add(Restrictions.eq("email", email));
        return (User) criteria.uniqueResult();
    }

    public Session getSession(){
        return sessionFactory.getCurrentSession();
    }

    public Criteria createEntityCriteria(){
        return getSession().createCriteria(User.class);
    }

}
