package app.captcha;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@PropertySource("classpath:captcha.properties")
public class CaptchaConfig {
    @Value("${google.recaptcha.site.key}")
    String googleRecaptchaSiteKey;
    @Value("${google.recaptcha.secret.key}")
    String googleRecaptchaSecretKey;
    @Bean
    public String googleRecaptchaSiteKey() {
        return this.googleRecaptchaSiteKey;
    }
    @Bean
    public String googleRecaptchaSecretKey() {
        return this.googleRecaptchaSecretKey;
    }
}
