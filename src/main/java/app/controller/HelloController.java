package app.controller;

import app.Entity.Role;
import app.Entity.User;
import app.captcha.VerifyRecaptcha;
import app.services.RoleService;
import app.services.UserService;
import app.validator.UserValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Collection;

@Controller
public class HelloController {

	@Autowired
	UserService userService;

    @Autowired
    RoleService roleService;

    @Autowired
    private UserValidator userValidator;



//	@RequestMapping(value = {"/welcome**" }, method = RequestMethod.GET)
//	public ModelAndView welcomePage() {
//
//		ModelAndView model = new ModelAndView();
//		model.addObject("title", "Spring Security Custom Login Form");
//		model.addObject("message", "This is welcome page!");
//		model.setViewName("hello");
//		return model;
//
//	}

//	@RequestMapping(value = "/admin**", method = RequestMethod.GET)
//	public ModelAndView adminPage() {
//
//		ModelAndView model = new ModelAndView();
//		model.addObject("title", "Spring Security Custom Login Form");
//		model.addObject("message", "This is protected page!");
//		model.setViewName("admin");
//
//		return model;
//
//	}

//	@RequestMapping(value = "/user**", method = RequestMethod.GET)
//	public ModelAndView userPage() {
//
//		ModelAndView model = new ModelAndView();
//		model.addObject("user title", "Spring Security Custom Login Form");
//		model.addObject("user message", "This is protected page!");
//		model.setViewName("user");
//
//		return model;
//
//	}


    @RequestMapping(value = {"/", "/login"}, method = RequestMethod.GET)
    public ModelAndView login(@RequestParam(value = "error", required = false) String error,
                              @RequestParam(value = "logout", required = false) String logout, HttpServletRequest request) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        Collection<GrantedAuthority> authorities = (Collection<GrantedAuthority>) authentication.getAuthorities();
        ModelAndView model = new ModelAndView();
        if (error != null) {
            model.addObject("error", "Invalid username and password!");
        }

        if (logout != null) {
            model.addObject("msg", "You've been logged out successfully.");
        }
        if (authorities.iterator().next().getAuthority().equals("ROLE_ADMIN")){
			return new ModelAndView("redirect:/homeadmin");
        }
        if (authorities.iterator().next().getAuthority().equals("ROLE_USER")){
            return new ModelAndView("redirect:/homeuser");
        }
        model.setViewName("login");

        return model;

    }

//	@RequestMapping(value = "/usernext**", method = RequestMethod.GET)
//	public ModelAndView usernextPage() {
//
//		ModelAndView model = new ModelAndView();
//		model.addObject("user title", "Spring Security Custom Login Form");
//		model.addObject("user message", "This is protected page!");
//		model.setViewName("usernext");
//
//		return model;
//
//	}


	@RequestMapping(value = "/homeadmin**", method = RequestMethod.GET)
	public ModelAndView homeadmin() {
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.addObject("userService", userService);
		modelAndView.setViewName("homeadmin");
		return modelAndView;
	}

	@RequestMapping(value = "/homeuser**", method = RequestMethod.GET)
	public ModelAndView homeuser() {
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName("homeuser");
		return modelAndView;
	}


    @RequestMapping(value = "/registration", method = RequestMethod.GET)
    public String registration(Model model) {
        model.addAttribute("userForm", new User());

        return "registration";
    }

    @RequestMapping(value = "/registration", method = RequestMethod.POST)
    public String registration(@ModelAttribute("userForm") User userForm, BindingResult bindingResult, Model model, @RequestParam(name="g-recaptcha-response") String recaptchaResponse) {
        userValidator.validate(userForm, bindingResult);

        if (bindingResult.hasErrors()) {
            return "registration";
        }

        try {
            if (!VerifyRecaptcha.verify(recaptchaResponse)) {
                return "registration";
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        Role role = roleService.findByName("user");
        userForm.setRole(role);
        userService.create(userForm);

        return "login";
    }

//	@RequestMapping(value="/logout", method = RequestMethod.GET)
//	public String logoutPage (HttpServletRequest request, HttpServletResponse response) {
//		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
//		if (auth != null){
//			new SecurityContextLogoutHandler().logout(request, response, auth);
//		}
//		return "redirect:/login?logout";
//	}


}