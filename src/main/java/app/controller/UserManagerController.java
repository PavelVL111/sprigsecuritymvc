package app.controller;

import app.Entity.Role;
import app.Entity.User;
import app.Model.ModelUser;
import app.services.RoleService;
import app.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.SimpleDateFormat;

@Controller
@ComponentScan("app")
public class UserManagerController {

    @Autowired
    UserService userService;
    @Autowired
    RoleService roleService;
//    @Autowired
//    MessageSource messageSource;

    @RequestMapping(value="/add", method = RequestMethod.GET)
    public ModelAndView add(@ModelAttribute("modelUser") ModelUser modelUser){
        User user = new User();
        Role role = new Role();
        user.setRole(role);
        User userchack = userService.findByLogin(modelUser.getUsername());

        if (userchack != null) {
            return new ModelAndView("redirect:/sendonaddchange?act=Add");
        }

        initUser(user, modelUser, role);

        if (!isValid(modelUser)) {
            return new ModelAndView("redirect:/sendonaddchange?act=Add");
        }

        if (userchack == null) {
            userService.create(user);
            return new ModelAndView("homeadmin", "userService", userService);
        }
        return new ModelAndView("/sendonaddchange?act=Add");

    }

    @RequestMapping(value = {"/edit"})
    public ModelAndView doPost(@ModelAttribute("modelUser") ModelUser modelUser) {
        User user = new User();
        Role role = new Role();
        user.setRole(role);

        if (!isValid(modelUser)) {
            return new ModelAndView("redirect:/sendonaddchange?act=Edit&login=" + modelUser.getUsername());
        }

        User userchack = userService.findByLogin(modelUser.getUsername());

        initUser(user, modelUser, role);

        user.setId(userchack.getId());
        userService.update(user);
        return new ModelAndView("homeadmin", "userService", userService);

    }

    boolean isValid(ModelUser modelUser){
        if (!modelUser.getPassword().equals(modelUser.getPasswordagain()) || !isDateValid(modelUser.getBirthday())) {
            return false;
        }
        return true;
    }

    @RequestMapping(value = {"/del"}, method = RequestMethod.GET)
    public ModelAndView delete(@RequestParam("login") String login) {
        User user = userService.findByLogin(login);
        userService.remove(user);
        return new ModelAndView("homeadmin", "userService", userService);
    }

    public static boolean isDateValid(String date) {
        SimpleDateFormat myFormat = new SimpleDateFormat("dd.MM.yyyy");
        myFormat.setLenient(false);
        try {
            myFormat.parse(date);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    private void initUser(User user, ModelUser modelUser, Role role) {
        user.setLogin(modelUser.getUsername());
        user.setPassword(modelUser.getPassword());
        user.setEmail(modelUser.getEmail());
        user.setFirstName(modelUser.getFirstname());
        user.setLastName(modelUser.getLastname());
        user.setBirthDay(modelUser.getBirthday());
        role = roleService.findByName(modelUser.getRole());
        user.setRole(role);
    }
}
