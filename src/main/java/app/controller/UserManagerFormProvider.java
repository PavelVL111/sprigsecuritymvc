package app.controller;

import app.Entity.User;
import app.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


@Controller
public class UserManagerFormProvider{

    @Autowired
    UserService userService;

    @RequestMapping(value="/sendonaddchange", method = RequestMethod.GET)
    public ModelAndView sendonaddchange (@RequestParam("act") String act, @RequestParam(value = "login", required = false) String login) {
        ModelAndView modelAndView = new ModelAndView();
        if (act.equals("Add")) {
            modelAndView.addObject("act", act);
            modelAndView.setViewName("addchangeuser");
            return modelAndView;
        }
        if (act.equals("Edit")) {
            User user = userService.findByLogin(login);
            modelAndView.addObject("act", act);
            modelAndView.addObject("user", user);
            modelAndView.setViewName("/addchangeuser");
            return modelAndView;
        }
        modelAndView.setViewName("/failure");
        return modelAndView;
    }
}
