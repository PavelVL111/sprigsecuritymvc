package app.converter;

import app.Entity.User;
import app.services.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

/**
 * A converter class used in views to map id's to actual userProfile objects.
 */
@Component
public class RoleToUserProfileConverter implements Converter<Object, User>{

	static final Logger logger = LoggerFactory.getLogger(RoleToUserProfileConverter.class);
	
	@Autowired
	UserService userService;

	/**
	 * Gets UserProfile by Id
	 * @see Converter#convert(Object)
	 */
	public User convert(Object element) {
		String login = ((String)element);
		User profile= userService.findByLogin(login);
		logger.info("Profile : {}",profile);
		return profile;
	}
	
}