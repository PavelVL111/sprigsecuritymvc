package app.handlertag;

import app.Entity.User;
import app.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.servlet.jsp.JspContext;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.SimpleTagSupport;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class TagHandler extends SimpleTagSupport {

    //    private static UserService userService;
    private UserService userService;

    public void doTag() throws IOException {
        JspContext jspContext = getJspContext();
//        if (userService == null){
//            userService = (UserService) jspContext.findAttribute("userService");
//        }
        List<User> list = userService.findAll();
        JspWriter out = jspContext.getOut();

        for (User user : list) {
            out.print("<tr><td nowrap>" + user.getLogin() +
                    "</td><td nowrap>" + user.getFirstName() +
                    "</td><td nowrap>" + user.getLastName() +
                    "</td><td>" + getAge(user.getBirthDay()) +
                    "</td><td>" + user.getRole().getName() +
                    "</td><td nowrap><a href=\"sendonaddchange?login=" +
                    user.getLogin() + "&act=Edit\">Edit</a> " +
                    "<a href=\"#\" id=\"" +
                    user.getLogin() + "\" name=\"show\" class=\"show\" value=\"/login=" +
                    user.getLogin() + "\" onclick=\"return confirmDelete();\">Delete</a></td></tr>");
        }
    }

    public static Integer getAge(String strBirthday)
    {
        SimpleDateFormat oldDateFormat = new SimpleDateFormat("dd.mm.yyyy", Locale.getDefault());
        Date birthday = null;
        try {
            birthday = oldDateFormat.parse(strBirthday);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Calendar dob = Calendar.getInstance();
        Calendar today = Calendar.getInstance();
        dob.setTime(birthday);
        dob.add(Calendar.DAY_OF_MONTH, -1);

        int age = today.get(Calendar.YEAR) - dob.get(Calendar.YEAR);
        if (today.get(Calendar.DAY_OF_YEAR) <= dob.get(Calendar.DAY_OF_YEAR)) {
            age--;
        }
        return age;
    }

    public UserService getBean(){
        return userService;
    }

    public void setBean(UserService userService){
        this.userService = userService;
    }

}
