package app.services;

import app.DAO.RoleDao;
import app.Entity.Role;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service("roleService")
@Transactional
public class RoleServiceImpl implements RoleService{

    @Autowired
    private RoleDao dao;

    @Override
    public void create(Role role) {
        dao.create(role);
    }

    @Override
    public void update(Role role) {
        dao.update(role);
    }

    @Override
    public void remove(Role role) {
        dao.remove(role);
    }

    @Override
    public Role findByName(String name) {
        return dao.findByName(name);
    }
}
