<%@ page import="java.util.List" %>
<%@ page import="app.Entity.User" %>
<%@ page import="app.Entity.Role" %>
<%@ page import="app.services.UserService" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="myprefix" uri="/WEB-INF/userlist.tld" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>

<html>
<head>
    <title>Title</title>
</head>
<body>

<%
    pageContext.setAttribute("userService", ((UserService) request.getSession().getAttribute("userService")));
    String username = (String) session.getAttribute("username");

%>
<div class="adminname" align="right">
    Admin
    <security:authorize access="isAuthenticated()">
        <security:authentication property="principal.username"/>
    </security:authorize>
    (<a href="logout">Logout</a>)
</div>
<div class="addref" align="left"><a href="/sendonaddchange?act=Add">Add new user</a></div>

<div class="tableuser">
    <table border=\"1\" cellspacing=\"0\" cellpadding=\"5\">
        <tr>
            <td nowrap>Login</td>
            <td nowrap>First Name</td>
            <td nowrap>Last Name</td>
            <td>Age</td>
            <td>Role</td>
            <td nowrap>Actions</td>
        </tr>
        <myprefix:userlist bean="${userService}"/>
    </table>
</div>

<style type="text/css">
    .adminname {
        margin-right: 2%;
        margin-top: 2%;
    }

    .addref {
        float: left;
        margin-top: 10%;
        margin-left: 10%;
    }

    .tableuser {
        float: left;
        margin-top: 12%;
        margin-left: -6%;
    }
</style>


<dialog>
    <p>Are you sure?</p>
    <button id="yes">Yes</button>
    <button id="close">No</button>
</dialog>

<script>
    var dialog = document.querySelector('dialog');
    var login;
    var arr = document.querySelectorAll('.show');
    var logout = document.querySelector('.logout');
    var xhr = new XMLHttpRequest();

    arr.forEach(function (item) {
        item.onclick = function () {
            dialog.show();
            login = this.id;
            console.log(login);
        };
    });

    document.querySelector('#yes').onclick = function () {
        xhr.open('GET' +
            '' +
            '', 'del?login=' + login);
        xhr.onload = function () {
            window.location.reload(true);
        };
        xhr.send();
    };


    document.querySelector('#close').onclick = function () {
        dialog.close();
    };
</script>

<style type="text/css">
    dialog {
        border: 1px solid rgba(0, 0, 0, 0.3);
        border-radius: 6px;
        box-shadow: 0 3px 7px rgba(0, 0, 0, 0.3);
    }
</style>

</body>
</html>