<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<div align="center">
    <h1>
        Hello,
        <security:authorize access="isAuthenticated()">
            <security:authentication property="principal.username"/>
        </security:authorize>
    </h1>
    <p>
        Click <a href="logout">here</a> to logout
</div>


<style>
    div {
        margin-top: 10%; /* Отступ сверху */
    }
</style>

</body>
</html>
