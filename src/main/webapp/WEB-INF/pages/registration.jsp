<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="th" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<script src='https://www.google.com/recaptcha/api.js'></script>

<html>
<head>

</head>

<body>
<form:form method="POST" modelAttribute="userForm">
    <spring:bind path="login">
        <p>Username <input type="text" name="login" id="login" class="field"/> <br/>
    </spring:bind>
    <spring:bind path="password">
        <p>Password<input type="password" name="password" id="password" class="field"/> <br/>
    </spring:bind>
    <spring:bind path="email">
        <p>Email<input type="text" name="email" id="email" class="field"/> <br/>
    </spring:bind>
    <spring:bind path="firstName">
        <p>First name<input type="text" name="firstName" id="firstName" class="field"/> <br/>
    </spring:bind>
    <spring:bind path="lastName">
        <p>Last name<input type="text" name="lastName" id="lastName" class="field"/> <br/>
    </spring:bind>
    <spring:bind path="birthDay">
        <p>Birthday<input type="text" name="birthDay" id="birthDay" class="field"/> <br/>
    </spring:bind>
    <input type="submit" value="Ok" id="submit" />
    <div class="g-recaptcha" data-sitekey="6LcoIGkUAAAAAItXMSyuwvIY6-894qJHA5z57fe1"></div>
</form:form>


</body>
</html>